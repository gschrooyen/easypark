package be.bit.EasyPark.Repos;

import be.bit.EasyPark.model.ParkingSpot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingSpotRepo extends JpaRepository<ParkingSpot, Integer> {
    public ParkingSpot getByParkingSpotId(int parkingSpotId);

}
