package be.bit.EasyPark.Repos;

import be.bit.EasyPark.model.ParkingAction;
import be.bit.EasyPark.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ParkingActionRepo extends JpaRepository<ParkingAction, Integer> {
    @Query(value = "select top 1 * from ParkingAction where parkingSpot_parkingSpotId = ?1 and endTimeStamp is null", nativeQuery = true)
    ParkingAction getActivePaOfSpot(int parkingspotId);

    @Query(value = "select * from ParkingAction where user_userId = ?1 and endTimeStamp is null", nativeQuery = true)
    List<ParkingAction> getActivePasOfUser(User usr);
}
