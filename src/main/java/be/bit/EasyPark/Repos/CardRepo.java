package be.bit.EasyPark.Repos;

import be.bit.EasyPark.model.Card;
import be.bit.EasyPark.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepo extends JpaRepository<Card, String> {
    public List<Card> getAllByUser(User user);
}
