package be.bit.EasyPark;

import be.bit.EasyPark.model.Card;
import be.bit.EasyPark.model.ParkingAction;
import be.bit.EasyPark.model.ParkingSpot;
import be.bit.EasyPark.model.User;
import be.bit.EasyPark.services.CardService;
import be.bit.EasyPark.services.ParkingActionService;
import be.bit.EasyPark.services.ParkingSpotService;
import be.bit.EasyPark.services.UserService;
import be.bit.EasyPark.ui.dto.CardScannedDto;
import be.bit.EasyPark.ui.dto.ParkingTakenDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class ParkingSpotListener {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final ParkingSpotService parkingSpotService;
    private final CardService cardService;
    private final ParkingActionService parkingActionService;
    private final UserService userService;

    public ParkingSpotListener(ParkingSpotService parkingSpotService, CardService cardService, ParkingActionService parkingActionService, UserService userService) {
        this.parkingSpotService = parkingSpotService;
        this.cardService = cardService;
        this.parkingActionService = parkingActionService;
        this.userService = userService;
    }

    @RabbitListener(queues = "Parking-Taken-Queue")
    public void receiveMessageForApp1(final ParkingTakenDto parkingTakenDto) {
        ParkingSpot ins1 = new ParkingSpot(51.23156, 3.15156, false, false, 0, "ten briele", "oudenaerde", "BE", "2950" );
        ParkingSpot ins2 = new ParkingSpot(51.326596, 3.5151, false, false, 0, "kerkstraat", "oudenaerde", "BE", "2950" );
        parkingSpotService.save(ins1);
        parkingSpotService.save(ins2);
        ParkingSpot ps = parkingSpotService.getParkingSpot(parkingTakenDto.getParkingId());
        if (ps.getBezet()){
            ps.setBezet(false);
            ps.setIngecheckt(false);
            ParkingAction pa = parkingActionService.getActiveAction(parkingTakenDto.getParkingId());
            pa.setEndTimeStamp(LocalDateTime.now());
            parkingActionService.saveAction(pa);
        }else{
            ps.setBezet(true);
            ps.setIngecheckt(false);
        }
        ParkingSpot psu = parkingSpotService.updateParkingspot(ps);
        LOGGER.info("parkingSpot " + psu.getParkingSpotId() + " is taken");
    }

    @RabbitListener(queues = "Card-Scanned-Queue")
    public void cardScannedMessage(final CardScannedDto cardScannedDto){
        ParkingSpot ps = parkingSpotService.getParkingSpot(cardScannedDto.getParkingId());
        if (!ps.getBezet()){
            LOGGER.warn("checkin into empty parking spot canceling the action");
            return;
        }
        User usr = cardService.getCard(cardScannedDto.getCardId()).getUser();
        if (parkingActionService.getActiveActionsByUser(usr).size() > 0){
            LOGGER.warn("user tried to double check in", usr);
            return;
        }
        ParkingAction pa = new ParkingAction(LocalDateTime.now(), ps, usr);
        parkingActionService.saveAction(pa);
    }
}
