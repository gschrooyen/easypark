package be.bit.EasyPark.services;

import be.bit.EasyPark.Repos.TagRepo;
import org.springframework.stereotype.Service;

@Service
public class TagService {
    private final TagRepo tagRepo;

    public TagService(TagRepo tagRepo) {
        this.tagRepo = tagRepo;
    }
}
