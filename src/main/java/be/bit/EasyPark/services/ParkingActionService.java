package be.bit.EasyPark.services;

import be.bit.EasyPark.Repos.ParkingActionRepo;
import be.bit.EasyPark.model.ParkingAction;
import be.bit.EasyPark.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParkingActionService {

    private final ParkingActionRepo parkingActionRepo;

    public ParkingActionService(ParkingActionRepo parkingActionRepo) {
        this.parkingActionRepo = parkingActionRepo;
    }

    public ParkingAction saveAction(ParkingAction parkingAction){
        return parkingActionRepo.save(parkingAction);
    }

    public ParkingAction getActiveAction(int parkingId) {
        return parkingActionRepo.getActivePaOfSpot(parkingId);
    }

    public List<ParkingAction> getActiveActionsByUser(User usr) {
        return parkingActionRepo.getActivePasOfUser(usr);
    }
}
