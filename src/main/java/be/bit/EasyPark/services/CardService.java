package be.bit.EasyPark.services;

import be.bit.EasyPark.Repos.CardRepo;
import be.bit.EasyPark.model.Card;
import be.bit.EasyPark.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {

    private final CardRepo cardRepo;

    public CardService(CardRepo cardRepo) {
        this.cardRepo = cardRepo;
    }

    public List<Card> getCardsOfUser(User usr) {
        return cardRepo.getAllByUser(usr);
    }

    public Card getCard(String cardId) {
        return cardRepo.getOne(cardId);
    }
}
