package be.bit.EasyPark.services;

import be.bit.EasyPark.Repos.ParkingSpotRepo;
import be.bit.EasyPark.model.ParkingSpot;
import org.springframework.stereotype.Service;

@Service
public class ParkingSpotService {
    private final ParkingSpotRepo parkingSpotRepo;

    public ParkingSpotService(ParkingSpotRepo parkingSpotRepo) {
        this.parkingSpotRepo = parkingSpotRepo;
    }

    public ParkingSpot getParkingSpot(int parkingSpotId){
        return parkingSpotRepo.getByParkingSpotId(parkingSpotId);
    }

    public ParkingSpot updateParkingspot(ParkingSpot ps) {
        return parkingSpotRepo.save(ps);
    }

    public void save(ParkingSpot ins1) {
        parkingSpotRepo.save(ins1);
    }
}
