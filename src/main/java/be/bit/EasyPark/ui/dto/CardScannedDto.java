package be.bit.EasyPark.ui.dto;

public class CardScannedDto {
    private String cardId;
    private int parkingId;

    public CardScannedDto() {
    }

    public CardScannedDto(String cardId, int parkingId) {
        this.cardId = cardId;
        this.parkingId = parkingId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }

    @Override
    public String toString() {
        return "CardScannedDto{" +
                "cardId=" + cardId +
                ", parkingId=" + parkingId +
                '}';
    }
}
