package be.bit.EasyPark.ui.dto;

import jdk.jfr.Unsigned;

public class ParkingTakenDto {
    private int parkingId;

    public ParkingTakenDto() {
    }

    public ParkingTakenDto(int parkingId) {
        this.parkingId = parkingId;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }
}
