package be.bit.EasyPark.model;

import javax.persistence.*;

@Entity
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int ratingId;
    private Double Score;
    @ManyToOne
    private ParkingSpot parkingSpot;
}
