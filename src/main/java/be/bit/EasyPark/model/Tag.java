package be.bit.EasyPark.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int tagId;
    private String Text;
    @ManyToMany
    private List<ParkingSpot> parkingSpots;
}
