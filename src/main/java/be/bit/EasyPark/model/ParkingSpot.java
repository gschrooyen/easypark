package be.bit.EasyPark.model;

import org.locationtech.spatial4j.context.SpatialContext;
import org.locationtech.spatial4j.shape.Point;

import javax.persistence.*;
import java.util.List;

@Entity
public class ParkingSpot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int parkingSpotId;
    @Column(nullable = false)
    private Double xCoord;
    @Column(nullable = false)
    private Double yCoord;
    @Column(nullable = false)
    private Boolean bezet;
    @Column(nullable = false)
    private Boolean ingecheckt;
    @ManyToMany
    private List<Tag> tags;
    private int Score;
    private String streetName;
    private String cityName;
    private String countryCode;
    private String zipCode;
    @OneToMany
    private List<Rating> ratings;

    public ParkingSpot() {
    }

    public ParkingSpot(Double xCoord, Double yCoord, Boolean bezet, Boolean ingecheckt, int score, String streetName, String cityName, String countryCode, String zipCode) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.bezet = bezet;
        this.ingecheckt = ingecheckt;
        this.Score = score;
        this.streetName = streetName;
        this.cityName = cityName;
        this.countryCode = countryCode;
        this.zipCode = zipCode;
    }

    public int getParkingSpotId() {
        return parkingSpotId;
    }

    public void setParkingSpotId(int parkingSpotId) {
        this.parkingSpotId = parkingSpotId;
    }

    public Double getxCoord() {
        return xCoord;
    }

    public void setxCoord(Double xCoord) {
        this.xCoord = xCoord;
    }

    public Double getyCoord() {
        return yCoord;
    }

    public void setyCoord(Double yCoord) {
        this.yCoord = yCoord;
    }

    public Boolean getBezet() {
        return bezet;
    }

    public void setBezet(Boolean bezet) {
        this.bezet = bezet;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Point getPoint(){
        return SpatialContext.GEO.getShapeFactory().pointXY(this.getxCoord(), this.getyCoord());
    }

    @Override
    public String toString() {
        return "ParkingSpot{" +
                "parkingSpotId=" + parkingSpotId +
                ", xCoord=" + xCoord +
                ", yCoord=" + yCoord +
                ", bezet=" + bezet +
                ", tags=" + tags +
                ", Score=" + Score +
                ", streetName='" + streetName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }

    public Boolean getIngecheckt() {
        return ingecheckt;
    }

    public void setIngecheckt(Boolean ingecheckt) {
        this.ingecheckt = ingecheckt;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }
}
