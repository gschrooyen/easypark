package be.bit.EasyPark.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class ParkingAction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int parkingActionId;
    @Column(nullable = false)
    private LocalDateTime startTimestamp;
    private LocalDateTime endTimeStamp;
    @ManyToOne
    private ParkingSpot parkingSpot;
    @ManyToOne
    private User user;

    public ParkingAction() {
    }

    public ParkingAction(LocalDateTime startTimestamp, ParkingSpot parkingSpot, User user) {
        this.startTimestamp = startTimestamp;
        this.endTimeStamp = endTimeStamp;
        this.parkingSpot = parkingSpot;
        this.user = user;
    }

    public int getParkingActionId() {
        return parkingActionId;
    }

    public void setParkingActionId(int parkingActionId) {
        this.parkingActionId = parkingActionId;
    }

    public LocalDateTime getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(LocalDateTime startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public LocalDateTime getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(LocalDateTime endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public ParkingSpot getParkingSpot() {
        return parkingSpot;
    }

    public void setParkingSpot(ParkingSpot parkingSpot) {
        this.parkingSpot = parkingSpot;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
