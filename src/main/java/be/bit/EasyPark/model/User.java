package be.bit.EasyPark.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int userId;
    private String firstName;
    private String lastName;
    private int Score;
    private String streetName;
    private String cityName;
    private String countryCode;
    private String zipCode;
    private int number;
    @ManyToMany
    private List<Card> cards;
    @OneToMany
    private List<ParkingAction> parkingActions;
}
