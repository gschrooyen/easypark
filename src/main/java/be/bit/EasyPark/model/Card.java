package be.bit.EasyPark.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Card {
    @Id
    @Column
    private String cardId;
    @ManyToOne
    private User user;
    @Column(nullable = false)
    private LocalDateTime validFrom;
    private LocalDateTime validThrough;

    public Card() {
    }

    public Card(User user, LocalDateTime validFrom, LocalDateTime validThrough) {
        this.user = user;
        this.validFrom = validFrom;
        this.validThrough = validThrough;
    }

    public String getCardid() {
        return cardId;
    }

    public void setCardid(String cardid) {
        this.cardId = cardid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidThrough() {
        return validThrough;
    }

    public void setValidThrough(LocalDateTime validThrough) {
        this.validThrough = validThrough;
    }
}
